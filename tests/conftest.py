import os
import pytest


os.environ["FLASK_ENV"] = "testing"
os.environ["SECRET_KEY"] = "secret"


@pytest.fixture(scope="session")
def faker():
    from faker import Faker

    Faker.seed(1234)
    faker_instance = Faker()

    return faker_instance


@pytest.fixture(scope="function")
def app_client():
    from cicd_example.app import app

    with app.test_client() as client:
        yield client


@pytest.fixture(scope="function")
def cli_client():
    from cicd_example.app import app

    client = app.test_cli_runner()
    yield client
