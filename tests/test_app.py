from werkzeug.useragents import UserAgent
from http import HTTPStatus

from cicd_example import version_str


def test_get_homepage(faker, app_client):
    user_agent_str = faker.user_agent()
    user_agent = UserAgent(user_agent_str)

    rv = app_client.get("/", headers={"User-Agent": user_agent})
    assert rv.status_code == HTTPStatus.OK
    assert rv.mimetype == "text/html"

    html = rv.data.decode("utf-8")
    assert user_agent.platform in html
    assert user_agent.browser in html
    assert user_agent.version in html
    assert version_str in html
