def test_generate_secret_key(cli_client):
    result = cli_client.invoke(args=['generate-secret-key'])
    out = result.output

    msg, key = out.strip().split(':')
    assert msg == "New secret key"
    assert len(key) == (32 * 2) + 1
