import os
import click

from flask import Flask, render_template, request, session
from werkzeug.useragents import UserAgent

from . import version_str


app = Flask(__name__)
app.secret_key = os.getenv("SECRET_KEY")


@app.cli.command("generate-secret-key")
@click.argument("nbytes", default=32)
def generate_secret_command(nbytes):
    import secrets

    new_secret_key = secrets.token_hex(nbytes)
    click.echo(f"New secret key: {new_secret_key}")


@app.route("/")
def get_homepage():
    ip_address = request.remote_addr
    useragent = UserAgent(request.headers.get("user-agent", ""))

    num_visits = session.get('visits', 1)
    session['visits'] = num_visits + 1

    return render_template(
        "homepage.html",
        version=version_str,
        ip_address=ip_address,
        useragent=useragent,
        num_visits=num_visits,
    )
