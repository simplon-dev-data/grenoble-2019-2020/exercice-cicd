# Exercice CI/CD

> Une simple application web utilisant Flask, GitLab-CI et Heroku pour illustrer l'intégration continue et le déploiement continu (CI/CD)

## Instruction générales

Pour effectuer les étapes de l'exercice, vous devez travailler sur la branche de votre prénom.
Pour cloner uniquement votre branche, tapez la commande suivante :

```
git clone https://gitlab.com/simplon-dev-data/exercice-cicd.git
git checkout -b <prenom> master
```

Pour envoyer la réponse sur le serveur, vous tapez :

```
git push origin <prenom>
```

Pour observer l'état d'avancement de vos pipeline de CI/CD :
https://gitlab.com/simplon-dev-data/exercice-cicd/pipelines

## Installation de l'environnement

1. `pipenv install -d`
2. `cp .example.env .env`
3. `pipenv shell`
4. `pytest`
5. `flask run`

## Etapes

### Etape 1 : intégration continue

1. Décommenter la partie `test` du fichier `.gitlab-ci.yml`
2. Commiter le changement :
```
git add .gitlab-ci.yml
git commit -m “Activation des tests dans gitlab-ci”
```
3. Pusher le changement :
```
git push origin <prenom>
```
4. Observer le processus sur GitLab-CI

### Etape 2 : déploiement continu

1. Décommenter la partie `deploy` du fichier `.gitlab-ci.yml``
2. Mettre à jour les variables dans `.gitlab-ci.yml` :
    - `HEROKU_APP` : le nom de votre application Heroku
    - `HEROKU_API_KEY` : à trouver dans la section "Account Settings" de Heroku
3. Dans Heroku, ajouter ces variables à l’app :
    - `FLASK_ENV`: `production`
    - `SECRET_KEY`: `<secret>` (vous pouvez le générer avec `flask generate-secret-key`)
3. Commiter le changement :
```
git add .gitlab-ci.yml
git commit -m "Activation du deploiement dans gitlab-ci"
```
4. Pusher le changement :
```
git push origin <prenom>
```
5. Observer le processus sur GitLab-CI
6. Observer le résultat sur Heroku

### Etape 3 : tester le workflow en cas d’introduction de bug

1. Supprimez la ligne 35 du fichier `cicd_example/app.py`
2. Commiter le changement :
```
git add .gitlab-ci.yml
git commit -m "Ne plus afficher les informations de connexion"
```
3. Pusher le changement :
```
git push origin <prenom>
```
4. Observer le processus sur GitLab-CI
5. Observer le résultat sur Heroku

## Documentation

- [GitLab-CI](https://docs.gitlab.com/ce/ci/README.html)
- [GitLab-CI YAML](https://docs.gitlab.com/ce/ci/yaml/)
- [Heroku CLI Install](https://devcenter.heroku.com/articles/heroku-cli#standalone-installation)
- [Heroku Authentication](https://devcenter.heroku.com/articles/authentication)
- [Heroku Deployment](https://devcenter.heroku.com/articles/git#for-an-existing-heroku-app)
